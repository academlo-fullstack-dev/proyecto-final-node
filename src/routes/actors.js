import express from "express";
import {getAll} from "../controllers/actors";
import {isAdmin, isEditor, isUser} from "../middlewares/roleAuth";
const router = express.Router();

router.get("/actors", getAll);

export default router;
