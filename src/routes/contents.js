import express from "express";
import {getAll} from "../controllers/contents";
import {isAdmin, isEditor, isUser} from "../middlewares/roleAuth";
const router = express.Router();

router.get("/contents", getAll);

export default router;
