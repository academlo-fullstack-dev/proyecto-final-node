'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EpisodeList extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  EpisodeList.init({
    seasonNum: DataTypes.INTEGER,
    episodeName: DataTypes.STRING,
    contentId: DataTypes.INTEGER,
    relaseDate: DataTypes.STRING,
    episodeRating: DataTypes.DECIMAL,
    episodeNum: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    episodeImdbLink: DataTypes.STRING,
    episodeScoreVotes: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'EpisodeList',
  });
  return EpisodeList;
};