'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Contents extends Model {
    static associate(models) {
      this.belongsToMany(models.Actors, {
        through: "ContentActors",
        foreignKey: "contentId"
      });
    }
  };
  Contents.init({
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    totalSeasons: DataTypes.INTEGER,
    imdbScore: DataTypes.DECIMAL,
    relaseDates: DataTypes.STRING,
    playTime: DataTypes.STRING,
    contentRatings: DataTypes.INTEGER,
    contentType: DataTypes.INTEGER,
    totalEpisodes: DataTypes.INTEGER,
    imdbLink: DataTypes.STRING,
    imdbScoreVotes: DataTypes.INTEGER,
    ratingDetails: DataTypes.JSON,
    languages: DataTypes.ARRAY(DataTypes.STRING)
  }, {
    sequelize,
    modelName: 'Contents',
  });
  return Contents;
};