'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ContentGenres', {
      genreId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Genres",
          foreignKey: "id"
        }
      },
      contentId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Contents",
          foreignKey: "id"
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ContentGenres');
  }
};