'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ContentDirectors', {      
      directorId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Directors",
          foreignKey: "id"
        }
      },
      contentId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Contents",
          foreignKey: "id"
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ContentDirectors');
  }
};