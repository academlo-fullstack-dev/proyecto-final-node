'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Contents', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      totalSeasons: {
        type: Sequelize.INTEGER
      },
      imdbScore: {
        type: Sequelize.DECIMAL
      },
      relaseDates: {
        type: Sequelize.STRING
      },
      playTime: {
        type: Sequelize.STRING
      },
      contentRatings: {
        type: Sequelize.INTEGER,
        references: {
          model: "ContentRatings",
          foreignKey: "id"
        }
      },
      contentType: {
        type: Sequelize.INTEGER,
        references: {
          model: "ContentTypes",
          foreignKey: "id"
        }
      },
      totalEpisodes: {
        type: Sequelize.INTEGER
      },
      imdbLink: {
        type: Sequelize.STRING
      },
      imdbScoreVotes: {
        type: Sequelize.INTEGER
      },
      ratingDetails: {
        type: Sequelize.JSON
      },
      languages: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Contents');
  }
};