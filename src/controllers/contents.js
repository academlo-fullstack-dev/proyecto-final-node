import { Contents, Actors } from "../models/";

export const getAll = async (req, res) => {
  try {
    let results = await Contents.findAll({
      attributes: { exclude: ["ratingDetails"] },
      include: [
        {
          model: Actors,
          attributes: [
            "name"
          ],
          through: {
            attributes: []
          }
        },
      ],
    });
    res.json(results);
  } catch (error) {
      console.log(error);
    res.status(500).json({
      message: "Hubo un error al tratar de procesar tu petición",
      error,
    });
  }
};
