import { Contents, Actors } from "../models/";

export const getAll = async (req, res) => {
  try {
    let results = await Actors.findAll({
      limit: 5,
      include: [
        {
          model: Contents,
          attributes: ["title"],
          through: {
            attributes: [],
          },
        },
      ],
    });
    res.json(results);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Hubo un error al tratar de procesar tu petición",
      error,
    });
  }
};
